Benvenuti al corso di Sistemi Operativi per l'anno accademico 2016-17.
In questo repository troverete il materiale aggiornato del corso, che verra'
aggiornato durante lo svolgimento dello stesso

Attraverso git potete ottenere una copia del materiale direttamente
sul vostro computer.

Come:
1. installate git (una volta sola)
    
    sudo apt get install git

2. scaricate il repo (una volta sola)
    
    git clone https://gitlab.com/grisetti/sistemi_operativi_2016_17.git
   
Al momento della sua invocazione, questo comando scarica una versione di tutto 
il materiale disponibile, nela directory sistemi_operativi_2016_17

Il materiale verra' aggiornato e completato durante il corso.
Per aggiornare il materiale e' sufficiente invocare il seguente comando
(dalla directory del repo)
    
    git pull
    
Se fate errori o sovrascrivete i files, e' sufficiente cancellare
la directory del repository e ripetere il passo 2. (git clone)

Buon divertimento,
    G.
    