#include <stdio.h>
#include "bankers_allocator.h"

BankersAllocator a;

// example of silbershatz

int allocated[][3]={
  {0,1,0},
  {2,0,0},
  {3,0,2}
};

int max [][3]={
  {7,5,3},
  {3,2,2},
  {9,0,2}
};

int available[]={3,3,2};

int main(int argc, char** argv) {
  int num_processes=3;
  int num_resources=3;

  // we fill in the data
  BankersAllocator_init(&a, num_processes, num_resources, available);
  for (int p=0; p<num_processes; ++p)
    for (int r=0; r<num_resources; ++r) {
      a.resources_max.row_ptrs[p][r]=max[p][r];
      a.resources_allocated.row_ptrs[p][r]=allocated[p][r];
    }

  int order[num_processes];

  int is_safe = BankersAllocator_isSafe(order, &a);
  printf("is_safe: %d\n", is_safe);
  printf("final_order: \n");
  IntArray_print(order, num_processes);
}
