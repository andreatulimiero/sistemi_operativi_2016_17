#include "int_array.h"
#include "int_matrix.h"


typedef struct BankersAllocator {
  int num_processes;
  int num_resources;
  int *resources_available;
  IntMatrix resources_max;
  IntMatrix resources_allocated;
} BankersAllocator;

void BankersAllocator_init(BankersAllocator* a,
			   int num_processes,
			   int num_resources,
			   int* resources_available);


int BankersAllocator_isSafe(int* safe_order, BankersAllocator* a);

int BankersAllocator_tryAlloc(int* safe_order,
			      BankersAllocator* a,
			      int p, int* requested_resources);
