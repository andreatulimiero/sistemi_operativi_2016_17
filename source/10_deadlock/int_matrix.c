#include <stdio.h>
#include <stdlib.h> 
#include <assert.h>
#include "int_matrix.h"
#include "int_array.h"

void IntMatrix_alloc(IntMatrix* m, int rows, int cols){
  int num_elements=rows*cols;
  m->rows=rows;
  m->cols=cols;
  m->buffer=IntArray_alloc(num_elements);
  m->row_ptrs=(int**) malloc( rows* sizeof(int*));
  for (int r=0; r<rows; ++r)
    m->row_ptrs[r]=m->buffer+(r*cols);
}


void IntMatrix_free(IntMatrix* m){
  free(m->row_ptrs);
  free(m->buffer);
}

void IntMatrix_copy(IntMatrix* dest, IntMatrix* src){
  if (dest->rows!=src->rows || dest->cols!=src->cols){
    IntMatrix_free(dest);
    IntMatrix_alloc(dest, src->rows, src->cols);
  }
  for (int r=0; r<src->rows; ++r)
    for (int c=0; c<src->cols; ++c)
      dest->row_ptrs[r][c]=src->row_ptrs[r][c];
}

void IntMatrix_print(IntMatrix* m) {
  printf("ROWS %d\nCOLS %d\n", m->rows, m->cols);
  for (int r=0; r< m->rows; ++r){
    for (int c=0; c<m->cols; ++c)
      printf("%d ", m->row_ptrs[r][c]);
    printf("\n");
  }
}

