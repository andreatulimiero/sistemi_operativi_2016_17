#include "int_array.h"
#include <stdio.h>
#include <stdlib.h> 


int* IntArray_alloc(int size) {
  return (int*) malloc(size* sizeof(int));
}

void IntArray_free(int* v){
  free(v);
}

void IntArray_set(int* v, int size, int value){
  for (; size; ++v, --size)
    *v=value;
}

void IntArray_sum(int* dest, int* src, int size){
  for (; size; ++dest, ++src, --size)
    *dest += *src;
}

void IntArray_sub(int* dest, int* src, int size){
  for (; size; ++dest, ++src, --size)
    *dest -= *src;
}

void IntArray_copy(int* dest, int* src, int size){
  for (; size; ++dest, ++src, --size)
    *dest = *src;
}


int IntArray_isLess(int* dest, int* src, int size){
  for (; size; ++dest, ++src, --size)
    if (*dest>*src)
      return 0;
  return 1;
}

void IntArray_print(int* v, int size) {
  printf("SIZE: %d\n", size);
  for (int c=0; c<size; ++c, ++v)
    printf("%d ", *v);
  printf("\n");
}
