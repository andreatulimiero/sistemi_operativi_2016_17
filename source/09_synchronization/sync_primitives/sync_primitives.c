#include "sync_primitives.h"

void Mutex_init(Mutex* m){
  m->lock=0;
}

void Mutex_lock(Mutex* m){
  //use intel atomic builtins
  while(__sync_bool_compare_and_swap (&m->lock, 0, 1));
}

void Mutex_unlock(Mutex* m){
  m->lock=0;
}

int Mutex_trylock(Mutex* m){
  //use intel atomic builtins
  return __sync_bool_compare_and_swap (&m->lock, 0, 1);
}


void Semaphore_init(Semaphore* s, int value){
  s->count=value;
  Mutex_init(&s->mutex);
}

void Semaphore_wait(Semaphore* s){
  volatile int blocked=0;
  // the one below is a horrible busy waiting,
  // in a real system one should suspend the calling process
  // and wake it until the resource becomes available
  while(! blocked) {  
    Mutex_lock(&s->mutex);
    if (s->count>0)
      blocked=1;
    else {
      --s->count;
      blocked=0;
    }
    Mutex_unlock(&s->mutex);
  }
}

int Semaphore_trywait(Semaphore* s){
  int blocked=0;
  Mutex_lock(&s->mutex);
  if (s->count>0)
    blocked=0;
  else {
    --s->count;
    blocked=1;
  }
  Mutex_unlock(&s->mutex);
  return blocked;
}

int Semaphore_value(Semaphore* s){
  int count=0;
  Mutex_lock(&s->mutex);
  count=s->count;
  Mutex_unlock(&s->mutex);
  return count;
}

void Semaphore_post(Semaphore* s){
  // notiing difficult here
  // however in a real system we would notify the
  // waiting processes that the resource is free and one of them
  // might be awakened
  Mutex_lock(&s->mutex);
  s->count++;
  Mutex_unlock(&s->mutex);
}
