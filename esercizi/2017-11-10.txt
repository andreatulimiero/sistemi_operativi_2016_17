1. Implement the function
   Reference 11_main_memory

   SegmentItem* Memory_moveSegment(SegmentItem* segment, uint32 new_base)

   that relocates a segment of memory to a new base page
   pointed by new_base

   The function returns a pointer to the manipulated item
   on success.
   0 otherwise

2. Implement the function
   Reference 11_main_memory

   SegmentItem* Memory_resizeSegment(SegmentItem* segment, uint32 new_limit)

   that resizes a segment of memory to match new limit. It potentially moves
   the base of the segment.
    
   0 otherwise (no free memory)

3. Implement a context swap between N contexts (argv!)
   Reference 04_contexts

